//import external dependences
import express from "express";

/**
 * This register_handler function will save the register details
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const register_handler = (req, res, next) => {
  const {
    firstname,
    lastname,
    email,
    phone_number,
    department,
    project,
    address,
  } = req.body;
  const param_count = Object.keys(req.body).length;
  if (param_count == 0) {
    res.status(400).send({ err: "you are not passing any paramters" });
  } else if (param_count >= 1 && param_count < 7) {
    res.status(400).send({ err: "you are missing some paramters" });
  } else if (param_count > 7) {
    res.status(400).send({ err: "you are passing more  paramters" });
  } else {
    let err_object = {};
    err_object = {
      ..._firstname_checker(firstname),
      ..._lastname_checker(lastname),
      ..._email_checker(email),
      ..._phone_no_checker(phone_number),
      ..._department_checker(department),
      ..._project_checker(project),
      ..._address_checker(address),
    };

    var count = 0;
    for (let key in err_object) {
      count++;
    }
    if (count > 0) {
      res.status(400).send({ err: err_object });
    } else {
      next();
    }
  }

  /**
   * This is a helper function to check and validate the firstname
   * @param {} firstname this is the parameter recieved to validate
   * @returns An object containing error messages.
   */
  function _firstname_checker(firstname) {
    const returnObject = {};
    if (firstname == undefined) {
      returnObject.firstname = "Firstname not given";
      return returnObject;
    }
    if (typeof firstname != "string") {
      returnObject.firstname = "Firstname must be a string";
      return returnObject;
    }
    if (firstname.length <= 8 && firstname.length > 6) {
      returnObject.firstname =
        "Firstname length must be in between 6 to 8 characters";
    }
    return returnObject;
  }

  /**
   * This is a helper function to check and validate the lastname
   * @param {} lastname this is the parameter recieved to validate
   * @returns An object containing error messages.
   */
  function _lastname_checker(lastname) {
    const returnObject = {};
    if (lastname == undefined) {
      returnObject.lastname = "lastname not given";
      return returnObject;
    }
    if (typeof lastname != "string") {
      returnObject.lastname = "lastname must be a string";
      return returnObject;
    }
    if (lastname.length <= 8 && lastname.length > 6) {
      returnObject.firstname =
        "Lastname length must be in between 6 to 8 characters";
    }
    return returnObject;
  }

  /**
   * This is a helper function to check and validate the email
   * @param {} email this is the parameter recieved to validate
   * @returns An object containing error messages.
   */
  function _email_checker(email) {
    const returnObject = {};
    //   /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if (email == undefined) {
      returnObject.email = "email not given";
      return returnObject;
    }
    if (typeof email != "string") {
      returnObject.email = "email must be a string";
      return returnObject;
    }
    if (
      !email
        .toLowerCase()
        .match(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )
    ) {
      returnObject.email = "email is supposed to be in valid format";
      return returnObject;
    }
    if (!email.endsWith("@gmail.com")) {
      returnObject.email = "Enter a valid mail that ends with @gmail.com";
      return returnObject;
    }
    return returnObject;
  }

  /**
   * This is a helper function to check and validate the phone_no
   * @param {} phone_no this is the parameter recieved to validate
   * @returns An object containing error messages.
   */
  function _phone_no_checker(phone_no) {
    const returnObject = {};
    if (phone_no == undefined) {
      returnObject.phone_no = "Phone Number Not given";
      return returnObject;
    }
    if (typeof phone_no != "number") {
      returnObject.phone_no = "Phone Number should be a Number type";
      return returnObject;
    }
    if (!Number.isInteger(phone_no)) {
      returnObject.phone_no =
        "Phone Number have no special characters points " + phone_no;
      return returnObject;
    }
    if (phone_no.toString().length != 10) {
      returnObject.phone_no = "Phone number should have 10 digits: " + phone_no;
      return returnObject;
    }

    return returnObject;
  }

  /**
   * This is a helper function to check and validate the role
   * @param {} Department this is the parameter recieved to validate
   * @returns An object containing error messages.
   */
  function _department_checker(department) {
    const returnObject = {};
    if (department == undefined) {
      returnObject.department = "department is not mentioned";
      return returnObject;
    }
    if (typeof department != "string") {
      returnObject.department = "department must be a string";
      return returnObject;
    }

    return returnObject;
  }

  /**
   * This is a helper function to check and validate the department
   * @param {} Project this is the parameter recieved to validate
   * @returns An object containing error messages.
   */
  function _project_checker(project) {
    const returnObject = {};
    if (project == undefined) {
      returnObject.project = "project is not mentioned";
      return returnObject;
    }
    if (typeof project != "string") {
      returnObject.project = "project must be a string";
      return returnObject;
    }

    return returnObject;
  }
  /**
   * This is a helper function to check and validate the Address
   * @param {} Address this is the parameter recieved to validate
   * @returns An object containing error messages.
   */
  function _address_checker(address) {
    const returnObject = {};
    if (address == undefined) {
      returnObject.address = "address is not mentioned";
      return returnObject;
    }
    if (typeof address != "string") {
      returnObject.address = "address must be a string";
      return returnObject;
    }

    return returnObject;
  }
};

export default register_handler;
