import express from "express";
import getData from "./APIS/getApi.js";
import post_details from "./APIS/postApi.js";
import update_details from "./APIS/putApi.js";
import delete_details from "./APIS/deleteApi.js";
import register_handler from "./Validations/register_validations.js";
const app=express()
app.use(express.json())


app.get('/:email',getData)
app.post('/register',register_handler,post_details)
app.put('/update',update_details)
app.delete('/delete/:email',delete_details)






app.listen(4000,()=>{
  console.log("sever running at 4000");
})