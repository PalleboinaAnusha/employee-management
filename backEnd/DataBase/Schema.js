// import mongoose
import mongoose from "mongoose";

/**
 * Schema for creating EmpModel
 */

const register_schema=new mongoose.Schema({
  firstname:              {  type:String,   required:true,  unique:true},
  lastname:               {  type:String,   required:true,  unique:true},
  email:                  {  type:String,   required:true,  unique:true},
  phone_number:           {  type:String,   required:true,  unique:true},
  department:             {  type:String,   required:true,  unique:true},
  project:                {  type:String,   required:true,  unique:true},
  address:                {  type:String}
});

//exporting schema
export default register_schema;