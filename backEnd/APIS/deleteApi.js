// external dependences
import express from "express";

//importing internal dependencies
// importing register_model 
import register_model from "../DataBase/Model.js";
/**
 *  This is the delete API to validate the QUERY params 
 * sent in this DELETE call
 * @param {*} req email params and checks for uniqueness
 * @param {*} res if the params allready exists in database sends error status_code and status_message
 */
function delete_details(req, res) {
  const { email } = req.params;
  console.log(email);
  register_model.findOne({ email }, (err, data) => {
    if (err) {
      res.status(500).send("server error");
    } else {
      console.log(data);
      if (data == null) {
        res.send("email not found");
      } else {
        register_model.deleteMany({ email }, (err, deleteobj) => {
          if (err) {
            res.send(err.message);
          } else {
            res.send(
              "Employee removed succesfully" + JSON.stringify(deleteobj)
            );
          }
        });
      }
    }
  });
}

export default delete_details;
