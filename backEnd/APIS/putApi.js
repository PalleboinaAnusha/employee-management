 // import external dependences
import express from "express"

// importing register_model
import register_model from "../DataBase/Model.js"

/**
 *  This is the  UPDATE API the QUERY params 
 * sent in this UPDATE call
 * @param {*} req firstname params and checks for uniqueness
 * @param {*} res if the params allready exists in database sends error status_code and status_message
 */

function update_details(req,res){
  const {email,firstname}=req.body
  register_model.findOne({email},(err,data)=>{
    if(err){
      res.status(500).send("internal server error")
    }
    else{
      //console.log(firstname);
      data.firstname=firstname
      data.save((err,savedObj)=>{
        if(err){
          res.status(400).send('unknown error')
        }
        else{
          res.send("firstname updated sucessfully"+savedObj)

        }

      })
     
    }
  })

}

export default update_details