//import external dependences
import express from 'express'

//import mongoose
import mongoose from 'mongoose'

//importing internal dependencies
// importing register_model 
import register_model from '../DataBase/Model.js'

/**
 *  This is the  post API the QUERY params 
 * sent in this post call
 * @param {*} req All params and checks for uniqueness
 * @param {*} res if the params allready exists in database sends error status_code and status_message
 */
function post_details(req,res){
  const details=req.body

  const posting_details=new register_model(details)
  posting_details.save((err,saved_object)=>{
    if(err){
      res.status(500).send("internal server error")
    }
    else{
      res.send(saved_object)
    }
  })
 

}
export default post_details