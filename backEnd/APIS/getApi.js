//import external dependences
import express from "express"

//importing internal dependencies
// importing register_model 
import register_model from "../DataBase/Model.js"

 /**
 *  This is the  getAPI the QUERY params 
 * sent in this get call
 * @param {*} req email params and checks for uniqueness
 * @param {*} res if the params allready exists in database sends error status_code and status_message
 */
function getData(req,res){
  const email=req.params
  register_model.find(email,(err,data)=>{
    if(err){
      res.status(500).send("email not found")
    }
    else{
      res.send(data)
    }
  })

}
export default getData 